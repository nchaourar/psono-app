import 'package:flutter_test/flutter_test.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/otp.dart';

void main() {
  group('helper', () {
    test('parse_url www domain', () {
      expect(
          helper.parseUrl('https://www.example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'https',
              authority: 'example.com',
              fullDomain: 'example.com',
              topDomain: 'example.com',
              port: 443,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url top lvl domain', () {
      expect(
          helper.parseUrl('https://example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'https',
              authority: 'example.com',
              fullDomain: 'example.com',
              topDomain: 'example.com',
              port: 443,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url sub domain', () {
      expect(
          helper.parseUrl('http://test.example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: 'test.example.com',
              fullDomain: 'test.example.com',
              topDomain: 'example.com',
              port: 80,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url sub domain with port', () {
      expect(
          helper.parseUrl('http://test.example.com:6000/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: 'test.example.com:6000',
              fullDomain: 'test.example.com',
              topDomain: 'example.com',
              port: 6000,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url ip', () {
      expect(
          helper.parseUrl('http://192.168.4.3/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: '192.168.4.3',
              fullDomain: '192.168.4.3',
              topDomain: '192.168.4.3',
              port: 80,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url ip with port', () {
      expect(
          helper.parseUrl('http://192.168.4.3:5000/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: '192.168.4.3:5000',
              fullDomain: '192.168.4.3',
              topDomain: '192.168.4.3',
              port: 5000,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parseTOTPUri', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/ACME:AzureDiamond?issuer=ACME&secret=NB2W45DFOIZA&algorithm=SHA1&digits=6&period=30');
      expect(otp.secret, 'NB2W45DFOIZA');
      expect(otp.type, 'totp');
      expect(otp.label, 'AzureDiamond');
      expect(otp.algorithm, 'SHA1');
      expect(otp.period, 30);
      expect(otp.digits, 6);
      expect(otp.issuer, 'ACME');
    });

    test('parseTOTPUri without issuer', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/IDCSB44EC208D?secret=7UYKS576YTKVHX4ERW5BHKDKZBMQA4ZB&counter=0&period=30');
      expect(otp.secret, '7UYKS576YTKVHX4ERW5BHKDKZBMQA4ZB');
      expect(otp.type, 'totp');
      expect(otp.label, 'IDCSB44EC208D');
      expect(otp.algorithm, 'SHA1');
      expect(otp.period, 30);
      expect(otp.digits, 6);
      expect(otp.issuer, null);
    });

    test('parseTOTPUri urlencoding', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/My%20A.B.%20Test%2C%20Inc.%3Ausername%40example.net?secret=123456SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Microsoft');
      expect(otp.secret, '123456SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA');
      expect(otp.type, 'totp');
      expect(otp.label, 'username@example.net');
      expect(otp.algorithm, 'SHA1');
      expect(otp.period, 30);
      expect(otp.digits, 6);
      expect(otp.issuer, 'Microsoft');
    });

    test('parseTOTPUri urlencoding v2', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/Amazon%3Atest%40theexample.com?secret=123456SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon');
      expect(otp.secret, '123456SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA');
      expect(otp.type, 'totp');
      expect(otp.label, 'test@theexample.com');
      expect(otp.algorithm, 'SHA1');
      expect(otp.period, 30);
      expect(otp.digits, 6);
      expect(otp.issuer, 'Amazon');
    });

    test('parseTOTPUri custom period', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/Amazon%3At%40t.com?period=45&secret=SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon');
      expect(otp.period, 45);
    });

    test('parseTOTPUri custom digits', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/Amazon%3At%40t.com?digits=8&secret=SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon');
      expect(otp.digits, 8);
    });

    test('parseTOTPUri custom algorithm', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/Amazon%3At%40t.com?algorithm=SHA256&secret=SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon');
      expect(otp.algorithm, 'SHA256');
    });

    test('parseTOTPUri algorithm caseinsensitive', () {
      OTP otp = helper.parseTOTPUri(
          'otpauth://totp/Amazon%3At%40t.com?algorithm=sha256&secret=SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon');
      expect(otp.algorithm, 'SHA256');
    });

    test('parseTOTPUri unknown algorithm', () {
      expect(
          () => helper.parseTOTPUri(
              'otpauth://totp/Amazon%3At%40t.com?algorithm=test256&secret=SOMETHINGLONGERTHANAUSUALSECRETNB2W45DFOIZA&issuer=Amazon'),
          throwsA(isA<Exception>()));
    });
  });
}
