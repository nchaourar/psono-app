import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/storage.dart';
import 'package:psono/model/config.dart';
import 'package:psono/screens/scan_qr/index.dart';

class ScanConfigScreen extends StatefulWidget {
  static String tag = 'scan-config-screen';
  @override
  _ScanConfigScreenState createState() => _ScanConfigScreenState();
}

class _ScanConfigScreenState extends State<ScanConfigScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool redirect = false;
  QRViewController controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        final String result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScanQR()),
        );

        Map configMap;
        Config config;
        // String result2 = """{
        //   "ConfigJson": {
        //     "backend_servers": [{
        //         "title": "Psono.pw",
        //         "domain": "example.com",
        //         "url": "https://example2.com"
        //       }
        //     ],
        //     "allow_custom_server": true,
        //     "allow_registration": true,
        //     "allow_lost_password": true,
        //     "authentication_methods": ["AUTHKEY", "LDAP"],
        //     "more_links": [{
        //         "href": "https://doc.psono.com/",
        //         "title": "DOCUMENTATION",
        //         "class": "fa-book"
        //       }, {
        //         "href": "privacy-policy.html",
        //         "title": "PRIVACY_POLICY",
        //         "class": "fa-user-secret"
        //       }, {
        //         "href": "https://www.psono.com",
        //         "title": "ABOUT_US",
        //         "class": "fa-info-circle"
        //       }
        //     ]
        //   }
        // }""";

        try {
          configMap = jsonDecode(result);
          config = Config.fromJson(configMap);
        } on FormatException {
          return;
        }

        await storage.write(
          key: 'config',
          value: result,
          iOptions: secureIOSOptions,
        );

        reduxStore.dispatch(
          new ConfigUpdatedAction(
            config,
          ),
        );
        if (config != null &&
            config.configJson != null &&
            config.configJson.backendServers != null &&
            config.configJson.backendServers.length > 0 &&
            config.configJson.backendServers[0].url  != null) {
          reduxStore.dispatch(
            new InitiateLoginAction(
              config.configJson.backendServers[0].url,
              "",
            ),
          );
        }
        Navigator.pushReplacementNamed(context, '/');
      });
    });

    return Scaffold();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
