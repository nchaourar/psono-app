import 'dart:io';

import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/lost_password/index.dart';
import 'package:psono/screens/scan_config/index.dart';
import 'package:psono/screens/register/index.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/storage.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_host.dart' as managerHost;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/theme.dart';

class SigninScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final _usernameController = TextEditingController(
    text: reduxStore.state.username,
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );
  final yubikeyOTPTokenController = TextEditingController(
    text: '',
  );
  final googleAuthenticatorCodeController = TextEditingController(
    text: '',
  );
  final duoCodeController = TextEditingController(
    text: '',
  );

  String _screen = 'default';
  List<String> _requiredMultifactors = [];
  bool multifactorEnabled = true;
  bool configExists = false;
  List<String> _authenticationMethods = [];
  bool _obscurePassword = true;
  apiClient.Info info;

  String _domainSuffix = '@' + (reduxStore.state.config != null &&
      reduxStore.state.config.configJson != null &&
      reduxStore.state.config.configJson.backendServers != null &&
      reduxStore.state.config.configJson.backendServers.length > 0 &&
      reduxStore.state.config.configJson.backendServers[0].domain != null ? reduxStore.state.config.configJson.backendServers[0].domain : helper.getDomain(reduxStore.state.serverUrl));

  @override
  void dispose() {
    _usernameController?.dispose();
    _passwordController?.dispose();
    _serverUrlController?.dispose();
    yubikeyOTPTokenController?.dispose();
    googleAuthenticatorCodeController?.dispose();
    duoCodeController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      )
    );

    final username = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "USERNAME"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 13.0, 20.0, 10.0),
          child: Text(
            _domainSuffix,
            style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 16.0),
          ),
        ),
      ),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "PASSWORD"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    void onChange() {
      if (_usernameController.text.contains('@')) {
        _domainSuffix = '';
      } else {
        _domainSuffix = '@' + (reduxStore.state.config != null &&
            reduxStore.state.config.configJson != null &&
            reduxStore.state.config.configJson.backendServers != null &&
            reduxStore.state.config.configJson.backendServers.length > 0 &&
            reduxStore.state.config.configJson.backendServers[0].domain != null ? reduxStore.state.config.configJson.backendServers[0].domain : helper.getDomain(_serverUrlController.text));
      }

      setState(() {
        _domainSuffix = _domainSuffix;
      });
    }

    _usernameController.addListener(onChange);
    _serverUrlController.addListener(onChange);

    final yubikeyOTPToken = TextFormField(
      controller: yubikeyOTPTokenController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "YUBIKEY_TOKEN"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final googleAuthenticatorCode = TextFormField(
      controller: googleAuthenticatorCodeController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "GOOGLE_AUTHENTICATOR_CODE"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final duoCode = TextFormField(
      controller: duoCodeController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "DUO_CODE"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    Function sendYubiKeyToken;
    Function sendDuoCodeBackground;
    Future<void> startNFC() async {
      try {
        NDEFMessage message = await NFC.readNDEF(once: true).first;
        // a yubikey NFC always contains the token at the end which is always 44 chars long
        String token;
        if (message.payload.length >= 44) {
          token = message.payload.substring(message.payload.length - 44);
        } else {
          token = message.payload;
        }
        yubikeyOTPTokenController.text = token;
        sendYubiKeyToken();
      } on NDEFReadingUnsupportedException {}
    }

    void showDuo2faForm() {
      sendDuoCodeBackground();
      setState(() {
        _screen = 'duo_2fa';
      });
    }

    void showGa2faForm() {
      setState(() {
        _screen = 'google_authenticator_2fa';
      });
    }

    void showYubikeyOtp2faForm() {
      startNFC();
      setState(() {
        _screen = 'yubikey_otp_2fa';
      });
    }

    void nextLoginStep(
        List<String> requiredMultifactors, bool solvedTwoFa) async {
      setState(() {
        _requiredMultifactors = requiredMultifactors;
      });

      if (requiredMultifactors.length == 0 ||
          (solvedTwoFa && multifactorEnabled == false)) {
        setState(() {
          _screen = 'loading';
        });
        await managerDatastoreUser.activateToken();
        Navigator.pushReplacementNamed(context, '/datastore/');
        return;
      }

      if (multifactorEnabled == false && requiredMultifactors.length > 1) {
        setState(() {
          _screen = 'pick_second_factor';
        });
      } else {
        String nextMultifactor = requiredMultifactors[0];

        if (nextMultifactor == 'google_authenticator_2fa') {
          showGa2faForm();
        } else if (nextMultifactor == 'duo_2fa') {
          showDuo2faForm();
        } else if (nextMultifactor == 'yubikey_otp_2fa') {
          showYubikeyOtp2faForm();
        } else {
          _showErrorDiaglog('ERROR',
              'Unknown Multifactor Method requested. Please upgrade your client.');
          setState(() {
            _screen = 'default';
          });
        }
      }
    }

    void initiateLogin(bool sendPlain, apiClient.Info info) async {
      List<String> requiredMultifactors;
      setState(() {
        _screen = 'loading';
      });
      try {
        requiredMultifactors = await managerDatastoreUser.login(
          _usernameController.text + _domainSuffix,
            (reduxStore.state.config != null &&
                reduxStore.state.config.configJson != null &&
                reduxStore.state.config.configJson.backendServers != null &&
                reduxStore.state.config.configJson.backendServers.length > 0 &&
                reduxStore.state.config.configJson.backendServers[0].domain != null ? reduxStore.state.config.configJson.backendServers[0].domain : helper.getDomain(_serverUrlController.text)),
          _passwordController.text,
          info,
          sendPlain,
        );
      } on apiClient.BadRequestException catch (e) {
        _showErrorDiaglog('ERROR', e.message);
        setState(() {
          _screen = 'default';
        });
        return;
      }

      if (requiredMultifactors.length == 0 && info.complianceEnforce2fa) {
        _showErrorDiaglog('SECOND_FACTOR_REQUIRED',
            'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
        setState(() {
          _screen = 'default';
        });
        return;
      }

      nextLoginStep(requiredMultifactors, false);
    }

    Future<List<String>> samlLogin(
        String samlTokenId, apiClient.Info info) async {
      return managerDatastoreUser.samlLogin(samlTokenId, info);
    }

    void initiateSamlLogin(SamlProvider provider, apiClient.Info info) async {
      var screenSize = MediaQuery.of(context).size;

      String samlTokenId =
          await managerDatastoreUser.samlInitiateLogin(provider, screenSize);

      List<String> requiredMultifactors;
      setState(() {
        _screen = 'loading';
      });
      try {
        requiredMultifactors = await samlLogin(samlTokenId, info);
      } on apiClient.BadRequestException catch (e) {
        _showErrorDiaglog('ERROR', e.message);
        setState(() {
          _screen = 'default';
        });
        return;
      }

      if (requiredMultifactors.length == 0 && info.complianceEnforce2fa) {
        _showErrorDiaglog('SECOND_FACTOR_REQUIRED',
            'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
        setState(() {
          _screen = 'default';
        });
        return;
      }

      nextLoginStep(requiredMultifactors, false);
    }

    Future<List<String>> oidcLogin(
        String oidcTokenId, apiClient.Info info) async {
      return managerDatastoreUser.oidcLogin(oidcTokenId, info);
    }

    void initiateOidcLogin(OidcProvider provider, apiClient.Info info) async {
      var screenSize = MediaQuery.of(context).size;

      String oidcTokenId =
          await managerDatastoreUser.oidcInitiateLogin(provider, screenSize);

      List<String> requiredMultifactors;
      setState(() {
        _screen = 'loading';
      });
      try {
        requiredMultifactors = await oidcLogin(oidcTokenId, info);
      } on apiClient.BadRequestException catch (e) {
        _showErrorDiaglog('ERROR', e.message);
        setState(() {
          _screen = 'default';
        });
        return;
      }

      if (requiredMultifactors.length == 0 && info.complianceEnforce2fa) {
        _showErrorDiaglog('SECOND_FACTOR_REQUIRED',
            'ADMINISTRATOR_REQUIRES_SECOND_FACTOR_GO_TO_WEBSITE');
        setState(() {
          _screen = 'default';
        });
        return;
      }

      nextLoginStep(requiredMultifactors, false);
    }

    sendYubiKeyToken = () async {
      setState(() {
        _screen = 'loading';
      });

      List<String> requiredMultifactors;
      try {
        requiredMultifactors = await managerDatastoreUser.yubikeyOtpVerify(
          yubikeyOTPTokenController.text,
        );
      } on apiClient.BadRequestException catch (e) {
        setState(() {
          _screen = 'yubikey_otp_2fa';
        });
        _showErrorDiaglog('ERROR', e.message);
        startNFC();
        return;
      }

      requiredMultifactors.remove('yubikey_otp_2fa');

      nextLoginStep(requiredMultifactors, true);
    };

    void sendGoogleAuthenticatorCode() async {
      setState(() {
        _screen = 'loading';
      });

      List<String> requiredMultifactors;
      try {
        requiredMultifactors = await managerDatastoreUser.gaVerify(
          googleAuthenticatorCodeController.text,
        );
      } on apiClient.BadRequestException catch (e) {
        setState(() {
          _screen = 'google_authenticator_2fa';
        });
        _showErrorDiaglog('ERROR', e.message);
        return;
      }

      requiredMultifactors.remove('google_authenticator_2fa');

      nextLoginStep(requiredMultifactors, true);
    }

    void sendDuoCode() async {
      setState(() {
        _screen = 'loading';
      });

      List<String> requiredMultifactors;
      try {
        requiredMultifactors = await managerDatastoreUser.duoVerify(
          duoCodeController.text,
        );
      } on apiClient.BadRequestException catch (e) {
        setState(() {
          _screen = 'duo_2fa';
        });
        _showErrorDiaglog('ERROR', e.message);
        return;
      }

      requiredMultifactors.remove('duo_2fa');

      nextLoginStep(requiredMultifactors, true);
    }

    sendDuoCodeBackground = () async {
      List<String> requiredMultifactors;
      try {
        requiredMultifactors = await managerDatastoreUser.duoVerify(
          duoCodeController.text,
        );
      } on apiClient.BadRequestException {
        return;
      }

      requiredMultifactors.remove('duo_2fa');

      nextLoginStep(requiredMultifactors, true);
    };

    final loginButton = component.BtnPrimary(
      onPressed: () async {
        try {
          await storage.write(
            key: 'serverUrl',
            value: _serverUrlController.text,
            iOptions: secureIOSOptions,
          );
          await storage.write(
            key: 'username',
            value: _usernameController.text + _domainSuffix,
            iOptions: secureIOSOptions,
          );

          reduxStore.dispatch(
            new InitiateLoginAction(
              _serverUrlController.text,
              _usernameController.text + _domainSuffix,
            ),
          );
          info = await apiClient.info();
        } on apiClient.ServiceUnavailableException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        var checkHostResult =
            await managerHost.checkHost(_serverUrlController.text, info);

        await storage.write(
          key: 'complianceDisableDeleteAccount',
          value: info.complianceDisableDeleteAccount.toString(),
          iOptions: secureIOSOptions,
        );

        reduxStore.dispatch(
          new SetVerifyKeyAction(
            checkHostResult.info.verifyKey,
            info.verifyKey,
            info.complianceDisableDeleteAccount,
          ),
        );

        setState(() {
          multifactorEnabled = checkHostResult.info.multifactorEnabled;
        });

        if (checkHostResult.status == 'matched') {
          if (info.authenticationMethods.contains('LDAP')) {
            setState(() {
              _screen = 'ask_send_plain';
            });
          } else {
            initiateLogin(false, info);
          }
          return;
        } else if (checkHostResult.status == 'new_server') {
          setState(() {
            _authenticationMethods = info.authenticationMethods;
            _screen = 'approve_new_server';
          });
          return;
        } else if (checkHostResult.status == 'signature_changed') {
          setState(() {
            _authenticationMethods = info.authenticationMethods;
            _screen = 'server_signature_changed';
          });
          return;
        } else {
          _showErrorDiaglog(
            'Invalid signature',
            'The server seems missconfigured. The signature provided by the server is cryptographically invalid.',
          );
        }
      },
      text: FlutterI18n.translate(context, "LOGIN"),
    );

    final abortButton = component.Btn(
      onPressed: () async {
        setState(() {
          _screen = 'default';
        });
      },
      text: FlutterI18n.translate(context, "ABORT"),
    );

    final server = TextFormField(
      keyboardType: TextInputType.url,
      controller: _serverUrlController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "SERVER"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final approveButton = RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      onPressed: () async {
        await managerHost.approveHost(
            reduxStore.state.serverUrl, reduxStore.state.verifyKey);
        if (_authenticationMethods.contains('LDAP')) {
          setState(() {
            _screen = 'ask_send_plain';
          });
        } else {
          initiateLogin(false, info);
        }
      },
      padding: EdgeInsets.all(12),
      color: primarySwatch.shade500,
      child: Text(FlutterI18n.translate(context, "APPROVE"),
          style: TextStyle(color: Colors.white)),
    );

    final cancelButton = FlatButton(
      onPressed: () {
        setState(() {
          _screen = 'default';
        });
      },
      child: Text(
        FlutterI18n.translate(context, "CANCEL"),
        style: TextStyle(color: Color(0xFFb1b6c1)),
      ),
    );

    final continueButton = FlatButton(
      onPressed: () async {
        await managerHost.approveHost(
          reduxStore.state.serverUrl,
          reduxStore.state.verifyKey,
        );
        if (_authenticationMethods.contains('LDAP')) {
          setState(() {
            _screen = 'ask_send_plain';
          });
        } else {
          initiateLogin(false, info);
        }
      },
      child: Text(
        FlutterI18n.translate(context, "IGNORE_AND_CONTINUE"),
        style: TextStyle(color: Color(0xFFb1b6c1)),
      ),
    );

    if (_screen == 'ask_send_plain') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(
                  context,
                  "SERVER_ASKS_FOR_YOUR_PLAINTEXT_PASSWORD",
                ),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 8.0),
              component.AlertWarning(
                text: FlutterI18n.translate(
                  context,
                  "ACCEPTING_THIS_WILL_SEND_YOUR_PLAIN_PASSWORD",
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnWarning(
                    onPressed: () async {
                      initiateLogin(true, info);
                    },
                    text: FlutterI18n.translate(context, "APPROVE_UNSAFE"),
                  ),
                  component.BtnSuccess(
                    onPressed: () async {
                      initiateLogin(false, info);
                    },
                    text: FlutterI18n.translate(context, "DECLINE_SAFE"),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'approve_new_server') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(context, "NEW_SERVER"),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 16.0),
              Text(
                FlutterI18n.translate(context, "FINGERPRINT_OF_THE_NEW_SERVER"),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1)),
              ),
              SizedBox(height: 16.0),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                decoration: new BoxDecoration(
                    color: Color(0xFFEEEEEE),
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(5.0),
                        topRight: const Radius.circular(5.0),
                        bottomLeft: const Radius.circular(5.0),
                        bottomRight: const Radius.circular(5.0))),
                child: new Text(
                  converter.toHex(reduxStore.state.verifyKey),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF555555),
                      backgroundColor: Color(0xFFEEEEEE)),
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  approveButton,
                  cancelButton,
                ],
              ),
              SizedBox(height: 8.0),
              component.AlertInfo(
                text: FlutterI18n.translate(
                    context, "IT_APPEARS_THAT_YOU_WANT_TO_CONNECT"),
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'server_signature_changed') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(context, "SERVER_SIGNATURE_CHANGED"),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 16.0),
              Text(
                FlutterI18n.translate(context, "THE_NEW_FINGERPRINT_IS"),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1)),
              ),
              SizedBox(height: 16.0),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                decoration: new BoxDecoration(
                    color: Color(0xFFEEEEEE),
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(5.0),
                        topRight: const Radius.circular(5.0),
                        bottomLeft: const Radius.circular(5.0),
                        bottomRight: const Radius.circular(5.0))),
                child: new Text(
                  converter.toHex(reduxStore.state.verifyKey),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF555555),
                      backgroundColor: Color(0xFFEEEEEE)),
                ),
              ),
              SizedBox(height: 16.0),
              Text(
                FlutterI18n.translate(context, "THE_OLD_FINGERPRINT_WAS"),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1)),
              ),
              SizedBox(height: 16.0),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                decoration: new BoxDecoration(
                    color: Color(0xFFEEEEEE),
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(5.0),
                        topRight: const Radius.circular(5.0),
                        bottomLeft: const Radius.circular(5.0),
                        bottomRight: const Radius.circular(5.0))),
                child: new Text(
                  converter.toHex(reduxStore.state.verifyKeyOld),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Color(0xFF555555),
                      backgroundColor: Color(0xFFEEEEEE)),
                ),
              ),
              SizedBox(height: 8.0),
              component.AlertDanger(
                text: FlutterI18n.translate(
                    context, "THE_SIGNATURE_OF_THE_SERVER_CHANGED"),
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  cancelButton,
                  continueButton,
                ],
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'pick_second_factor') {
      List<Widget> children = <Widget>[
        logo,
        SizedBox(height: 56.0),
        Text(
          FlutterI18n.translate(
            context,
            "PICK_SECOND_FACTOR",
          ),
          textAlign: TextAlign.left,
          style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
        ),
        SizedBox(height: 8.0),
      ];

      if (_requiredMultifactors.contains('google_authenticator_2fa')) {
        children.add(
          component.BtnPrimary(
            onPressed: () async {
              showGa2faForm();
            },
            text: FlutterI18n.translate(context, "GOOGLE_AUTHENTICATOR"),
          ),
        );
      }

      if (_requiredMultifactors.contains('yubikey_otp_2fa')) {
        children.add(
          component.BtnPrimary(
            onPressed: () async {
              showYubikeyOtp2faForm();
            },
            text: FlutterI18n.translate(context, "YUBIKEY"),
          ),
        );
      }

      if (_requiredMultifactors.contains('duo_2fa')) {
        children.add(
          component.BtnPrimary(
            onPressed: () async {
              showDuo2faForm();
            },
            text: FlutterI18n.translate(context, "DUO"),
          ),
        );
      }

      children.add(abortButton);

      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: children,
          ),
        ),
      );
    } else if (_screen == 'yubikey_otp_2fa') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(
                  context,
                  "ENTER_YOUR_YUBIKEY_OTP_TOKEN",
                ),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 8.0),
              yubikeyOTPToken,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnPrimary(
                    onPressed: () async {
                      sendYubiKeyToken();
                    },
                    text: FlutterI18n.translate(context, "SEND"),
                  ),
                  abortButton,
                ],
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'google_authenticator_2fa') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(
                  context,
                  "ENTER_YOUR_GOOGLE_AUTHENTICATOR_CODE",
                ),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 8.0),
              googleAuthenticatorCode,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnPrimary(
                    onPressed: () async {
                      sendGoogleAuthenticatorCode();
                    },
                    text: FlutterI18n.translate(context, "SEND"),
                  ),
                  abortButton,
                ],
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'duo_2fa') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              Text(
                FlutterI18n.translate(
                  context,
                  "APPROVE_THE_PUSH_NOTIFICATION",
                ),
                textAlign: TextAlign.left,
                style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 24.0),
              ),
              SizedBox(height: 8.0),
              duoCode,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnPrimary(
                    onPressed: () async {
                      sendDuoCode();
                    },
                    text: FlutterI18n.translate(context, "SEND"),
                  ),
                  abortButton,
                ],
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else {
      bool allowRegistration = true;
      bool allowCustomServer = true;
      bool allowLostPassword = true;
      bool authkeyEnabled = false;
      bool ldapEnabled = false;
      bool samlEnabled = false;
      bool oidcEnabled = false;
      List<SamlProvider> samlProvider = [];
      List<OidcProvider> oidcProvider = [];

      if (reduxStore.state.config != null &&
          reduxStore.state.config.configJson != null) {
        setState(() {
          configExists = true;
        });
        ConfigJson configJson = reduxStore.state.config.configJson;
        if (configJson.allowRegistration != null) {
          allowRegistration = configJson.allowRegistration;
        }
        if (configJson.allowCustomServer != null) {
          allowCustomServer = configJson.allowCustomServer;
        }
        if (configJson.allowLostPassword != null) {
          allowLostPassword = configJson.allowLostPassword;
        }
        if (configJson.authenticationMethods != null) {
          authkeyEnabled = configJson.authenticationMethods.contains('AUTHKEY');
          ldapEnabled = configJson.authenticationMethods.contains('LDAP');
          samlEnabled = configJson.authenticationMethods.contains('SAML');
          oidcEnabled = configJson.authenticationMethods.contains('OIDC');
        } else {
          authkeyEnabled = true;
        }
        if (configJson.samlProvider != null) {
          samlProvider = configJson.samlProvider;
        }
        if (configJson.oidcProvider != null) {
          oidcProvider = configJson.oidcProvider;
        }
      } else {
        authkeyEnabled = true;
        authkeyEnabled = true;
      }

      List<Widget> buttonBar = <Widget>[
        Expanded(
          child: loginButton,
        ),
      ];

      if (allowRegistration) {
        buttonBar.add(
          Expanded(
            child: FlatButton(
              child: new Text(FlutterI18n.translate(context, "REGISTER")),
              textColor: Color(0xFFb1b6c1),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => RegisterScreen(),
                  ),
                );
              },
            ),
          ),
        );
      }

      List<Widget> children = <Widget>[
        logo,
        SizedBox(height: 24.0),
      ];

      if (oidcEnabled) {
        for (var i = 0; i < oidcProvider.length; i++) {
          if (oidcProvider[i].title != null && oidcProvider[i].title != '') {
            children.add(
              new Text(
                oidcProvider[i].title,
                style: TextStyle(color: Color(0xFFb1b6c1)),
              ),
            );
            children.add(SizedBox(height: 8.0));
          }
          String buttonTitle = FlutterI18n.translate(context, "LOGIN");
          if (oidcProvider[i].buttonName != null &&
              oidcProvider[i].buttonName != '') {
            buttonTitle = oidcProvider[i].buttonName;
          }
          children.add(
            component.BtnPrimary(
              text: buttonTitle,
              onPressed: () async {
                try {
                  await storage.write(
                    key: 'serverUrl',
                    value: _serverUrlController.text,
                    iOptions: secureIOSOptions,
                  );
                  await storage.write(
                    key: 'username',
                    value: _usernameController.text + _domainSuffix,
                    iOptions: secureIOSOptions,
                  );

                  reduxStore.dispatch(
                    new InitiateLoginAction(
                      _serverUrlController.text,
                      _usernameController.text + _domainSuffix,
                    ),
                  );
                  info = await apiClient.info();
                } on apiClient.ServiceUnavailableException {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "SERVER_OFFLINE"),
                    FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                  );
                  return;
                } on HandshakeException {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                    FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
                  );
                  return;
                } catch (e) {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "SERVER_OFFLINE"),
                    FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                  );
                  return;
                }

                var checkHostResult = await managerHost.checkHost(
                    _serverUrlController.text, info);

                await storage.write(
                  key: 'complianceDisableDeleteAccount',
                  value: info.complianceDisableDeleteAccount.toString(),
                  iOptions: secureIOSOptions,
                );

                reduxStore.dispatch(
                  new SetVerifyKeyAction(
                    checkHostResult.info.verifyKey,
                    info.verifyKey,
                    info.complianceDisableDeleteAccount,
                  ),
                );

                setState(() {
                  multifactorEnabled = checkHostResult.info.multifactorEnabled;
                });

                if (checkHostResult.status == 'matched') {
                  initiateOidcLogin(oidcProvider[i], info);
                  return;
                } else if (checkHostResult.status == 'new_server') {
                  setState(() {
                    _authenticationMethods = info.authenticationMethods;
                    _screen = 'approve_new_server';
                  });
                  return;
                } else if (checkHostResult.status == 'signature_changed') {
                  setState(() {
                    _authenticationMethods = info.authenticationMethods;
                    _screen = 'server_signature_changed';
                  });
                  return;
                } else {
                  _showErrorDiaglog(
                    'Invalid signature',
                    'The server seems missconfigured. The signature provided by the server is cryptographically invalid.',
                  );
                }
              },
            ),
          );
          children.add(SizedBox(height: 8.0));
        }
      }

      if (oidcEnabled &&
          oidcProvider.length > 0 &&
          (samlEnabled || authkeyEnabled || ldapEnabled)) {
        children.add(
          Divider(
            color: Color(0xFF0f1118),
          ),
        );
        children.add(SizedBox(height: 8.0));
      }

      if (samlEnabled) {
        for (var i = 0; i < samlProvider.length; i++) {
          if (samlProvider[i].title != null && samlProvider[i].title != '') {
            children.add(
              new Text(
                samlProvider[i].title,
                style: TextStyle(color: Color(0xFFb1b6c1)),
              ),
            );
            children.add(SizedBox(height: 8.0));
          }
          String buttonTitle = FlutterI18n.translate(context, "LOGIN");
          if (samlProvider[i].buttonName != null &&
              samlProvider[i].buttonName != '') {
            buttonTitle = samlProvider[i].buttonName;
          }
          children.add(
            component.BtnPrimary(
              text: buttonTitle,
              onPressed: () async {
                try {
                  await storage.write(
                    key: 'serverUrl',
                    value: _serverUrlController.text,
                    iOptions: secureIOSOptions,
                  );
                  await storage.write(
                    key: 'username',
                    value: _usernameController.text + _domainSuffix,
                    iOptions: secureIOSOptions,
                  );

                  reduxStore.dispatch(
                    new InitiateLoginAction(
                      _serverUrlController.text,
                      _usernameController.text + _domainSuffix,
                    ),
                  );
                  info = await apiClient.info();
                } on apiClient.ServiceUnavailableException {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "SERVER_OFFLINE"),
                    FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                  );
                  return;
                } on HandshakeException {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                    FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
                  );
                  return;
                } catch (e) {
                  _showErrorDiaglog(
                    FlutterI18n.translate(context, "SERVER_OFFLINE"),
                    FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
                  );
                  return;
                }

                var checkHostResult = await managerHost.checkHost(
                    _serverUrlController.text, info);

                await storage.write(
                  key: 'complianceDisableDeleteAccount',
                  value: info.complianceDisableDeleteAccount.toString(),
                  iOptions: secureIOSOptions,
                );

                reduxStore.dispatch(
                  new SetVerifyKeyAction(
                    checkHostResult.info.verifyKey,
                    info.verifyKey,
                    info.complianceDisableDeleteAccount,
                  ),
                );

                setState(() {
                  multifactorEnabled = checkHostResult.info.multifactorEnabled;
                });

                if (checkHostResult.status == 'matched') {
                  initiateSamlLogin(samlProvider[i], info);
                  return;
                } else if (checkHostResult.status == 'new_server') {
                  setState(() {
                    _authenticationMethods = info.authenticationMethods;
                    _screen = 'approve_new_server';
                  });
                  return;
                } else if (checkHostResult.status == 'signature_changed') {
                  setState(() {
                    _authenticationMethods = info.authenticationMethods;
                    _screen = 'server_signature_changed';
                  });
                  return;
                } else {
                  _showErrorDiaglog(
                    'Invalid signature',
                    'The server seems missconfigured. The signature provided by the server is cryptographically invalid.',
                  );
                }
              },
            ),
          );
          children.add(SizedBox(height: 8.0));
        }
      }

      if (samlEnabled &&
          samlProvider.length > 0 &&
          (authkeyEnabled || ldapEnabled)) {
        children.add(
          Divider(
            color: Color(0xFF0f1118),
          ),
        );
        children.add(SizedBox(height: 8.0));
      }

      if (authkeyEnabled || ldapEnabled) {
        children.add(username);
        children.add(SizedBox(height: 8.0));
        children.add(password);
        children.add(SizedBox(height: 8.0));
        children.add(
          Row(
            children: buttonBar,
          ),
        );
      }

      if (allowCustomServer) {
        children.add(SizedBox(height: 24.0));
        children.add(server);
      }
      children.add(SizedBox(height: 16.0));

      TextSpan lostPassword = new TextSpan(
        text: FlutterI18n.translate(context, "LOST_PASSWORD") + '?',
        style: new TextStyle(color: Color(0xFF666666)),
        recognizer: new TapGestureRecognizer()
          ..onTap = () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LostPasswordScreen(),
              ),
            );
          },
      );

      TextSpan privacyPolicy = new TextSpan(
        text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
        style: new TextStyle(color: Color(0xFF666666)),
        recognizer: new TapGestureRecognizer()
          ..onTap = () async {
            final url = Uri.parse('https://www.psono.pw/privacy-policy.html');
            if (await canLaunchUrl(url)) {
              await launchUrl(url);
            } else {
              throw 'Could not launch $url';
            }
          },
      );

      TextSpan scanConfig = new TextSpan(
        text: FlutterI18n.translate(context, "SCAN_CONFIG") + '?',
        style: new TextStyle(color: Color(0xFF666666)),
        recognizer: new TapGestureRecognizer()
          ..onTap = () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ScanConfigScreen(),
              ),
            );
          },
      );

      TextSpan forgetConfig = new TextSpan(
        text: FlutterI18n.translate(context, "FORGET_CONFIG") + '?',
        style: new TextStyle(color: Color(0xFF666666)),
        recognizer: new TapGestureRecognizer()
          ..onTap = () {
            print("FORGET_CONFIG");
            storage.delete(key: "config");
            reduxStore.dispatch(
              new ConfigUpdatedAction(
                new Config(),
              ),
            );
            setState(() {
              configExists = false;
            });
            onChange();
          },
      );

      List<InlineSpan> footerLinks = <InlineSpan>[];

      if (allowLostPassword) {
        footerLinks.add(lostPassword);
        footerLinks.add(
          new TextSpan(
            text: '   ',
          ),
        );
      }
      footerLinks.add(privacyPolicy);
      if (allowCustomServer && !configExists) {
        footerLinks.add(
          new TextSpan(
            text: '   ',
          ),
        );
        footerLinks.add(scanConfig);
      }

      if (configExists) {
        footerLinks.add(
          new TextSpan(
            text: '   ',
          ),
        );
        footerLinks.add(forgetConfig);
      }

      Center footer = new Center(
        child: new RichText(
          text: new TextSpan(
            children: footerLinks,
          ),
        ),
      );
      children.add(footer);

      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: children,
          ),
        ),
      );
    }
  }
}
