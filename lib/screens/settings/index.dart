import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:psono/screens/custom_drawer.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/autofill.dart' as autofillService;
import './autofill_ios.dart';
import './autofill_android.dart';
import './datastore.dart';
import './lockscreen.dart';
import './password_generator.dart';

class SettingsScreen extends StatefulWidget {
  static String tag = 'settings-screen';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _autofillSupported = false;

  Future<void> initStateAsync() async {
    bool autofillSupported = await autofillService.isSupported();

    setState(() {
      _autofillSupported = autofillSupported;
    });
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      initStateAsync();
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> settings = [
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'PASSWORD_GENERATOR')),
          leading: Icon(component.FontAwesome.calculator),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingPasswordGenerator(),
              ),
            );
          },
        ),
      ),
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'LOCKSCREEN')),
          leading: Icon(component.FontAwesome.unlock_alt),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingLockScreen(),
              ),
            );
          },
        ),
      ),
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'DATASTORES')),
          leading: Icon(component.FontAwesome.database),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingDatastoreScreen(),
              ),
            );
          },
        ),
      ),
    ];

    if (_autofillSupported && Platform.isAndroid) {
      settings.add(Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'AUTOFILL_SERVICE')),
          leading: Icon(component.FontAwesome.cogs),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillAndroidScreen(),
              ),
            );
          },
        ),
      ));
    }

    if (_autofillSupported && Platform.isIOS) {
      settings.add(Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'AUTOFILL_SERVICE')),
          leading: Icon(component.FontAwesome.cogs),
          trailing: Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SettingAutofillIOSScreen(),
              ),
            );
          },
        ),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, 'SETTINGS')),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
          padding: new EdgeInsets.all(10.0),
          child: new ListView(
            children: ListTile.divideTiles(
              context: context,
              tiles: settings,
            ).toList(),
          )),
      drawer: CustomDrawer(),
    );
  }
}
