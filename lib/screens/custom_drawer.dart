import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:psono/theme.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class CustomDrawer extends StatefulWidget {
  static String tag = 'datastore-screen';
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  String _versionInfo = '';

  Future<void> loadVersionInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;

    setState(() {
      _versionInfo = appName + ": " + version + " (build " + buildNumber + ")";
    });
  }

  @override
  void initState() {
    super.initState();
    loadVersionInfo();
  }

  @override
  Widget build(BuildContext context) {
    var route = ModalRoute.of(context);
    String routeName = '';
    if (route != null) {
      routeName = route.settings.name;
    } else {
      routeName = '';
    }

    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color(0xFF151f2b),
      ),
      child: Drawer(
        child: new Column(
          children: <Widget>[
            new Container(
              color: Color(0xFFFFFFFF),
              width: double.infinity,
              height: 98.0,
              child: new DrawerHeader(
                  margin: EdgeInsets.all(0.0),
                  padding: EdgeInsets.all(10.0),
                  child: Hero(
                    tag: 'hero',
                    child: SvgPicture.asset(
                      'assets/images/logo_green.svg',
                      semanticsLabel: 'Psono Logo',
                      height: 70.0,
                    ),
                  )),
            ),
            new Container(
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "NAVIGATION"),
                  style: TextStyle(
                    color: Color(0xFFb1b6c1),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            new Container(
              decoration: new BoxDecoration(
                color: routeName == '/datastore/'
                    ? primarySwatch.shade500
                    : Color(0xFF151f2b),
                borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "HOME"),
                  style: TextStyle(
                    color: routeName == '/datastore/'
                        ? Color(0xFFFFFFFF)
                        : Color(0xFFb1b6c1),
                  ),
                ),
                leading: new Icon(
                  component.FontAwesome.home,
                  color: routeName == '/datastore/'
                      ? Color(0xFFFFFFFF)
                      : Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/datastore/');
                },
              ),
            ),
            new Container(
              decoration: new BoxDecoration(
                color: routeName == '/settings/'
                    ? primarySwatch.shade500
                    : Color(0xFF151f2b),
                borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "SETTINGS"),
                  style: TextStyle(
                    color: routeName == '/settings/'
                        ? Color(0xFFFFFFFF)
                        : Color(0xFFb1b6c1),
                  ),
                ),
                leading: new Icon(
                  component.FontAwesome.cogs,
                  color: routeName == '/settings/'
                      ? Color(0xFFFFFFFF)
                      : Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/settings/');
                },
              ),
            ),
            new Container(
              decoration: new BoxDecoration(
                color: routeName == '/account/'
                    ? primarySwatch.shade500
                    : Color(0xFF151f2b),
                borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "ACCOUNT"),
                  style: TextStyle(
                    color: routeName == '/account/'
                        ? Color(0xFFFFFFFF)
                        : Color(0xFFb1b6c1),
                  ),
                ),
                leading: new Icon(
                  component.FontAwesome.sliders,
                  color: routeName == '/account/'
                      ? Color(0xFFFFFFFF)
                      : Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/account/');
                },
              ),
            ),
            new Container(
              color: Color(0xFF151f2b),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "LOGOUT"),
                  style: TextStyle(
                    color: Color(0xFFb1b6c1),
                  ),
                ),
                leading: new Icon(
                  component.FontAwesome.sign_out,
                  color: Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  managerDatastoreUser.logout();
                  Navigator.pushReplacementNamed(context, '/signin/');
                },
              ),
            ),
            new Expanded(
              child: new Align(
                alignment: Alignment.bottomLeft,
                child: new Container(
                  child: ListTile(
                    title: new Text(
                      this._versionInfo,
                      style: TextStyle(color: Color(0xFF444851)),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
