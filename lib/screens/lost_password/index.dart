import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/recovery_enable.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/storage.dart';
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/redux/actions.dart';

class LostPasswordScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _LostPasswordScreenState createState() => _LostPasswordScreenState();
}

class _LostPasswordScreenState extends State<LostPasswordScreen> {
  final _usernameController = TextEditingController(
    text: '',
  );
  final _code1Controller = TextEditingController(
    text: '',
  );
  final _code2Controller = TextEditingController(
    text: '',
  );
  final _wordsController = TextEditingController(
    text: '',
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  String _screen = 'default';
  String _recoveryCode = 'default';
  RecoveryEnable _recoveryData;
  bool _obscurePassword = true;
  apiClient.Info info;

  String _domainSuffix = '@' + helper.getDomain(reduxStore.state.serverUrl);

  @override
  void dispose() {
    _usernameController?.dispose();
    _code1Controller?.dispose();
    _code2Controller?.dispose();
    _wordsController?.dispose();
    _passwordController?.dispose();
    _passwordRepeatController?.dispose();
    _serverUrlController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    final username = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "USERNAME"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 13.0, 20.0, 10.0),
          child: Text(
            _domainSuffix,
            style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 16.0),
          ),
        ),
      ),
    );

    final code1 = TextFormField(
      controller: _code1Controller,
      autofocus: false,
      decoration: InputDecoration(
        hintText: "DdSLuiDcPuY2F",
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final code2 = TextFormField(
      controller: _code2Controller,
      autofocus: false,
      decoration: InputDecoration(
        hintText: "Dsxf82sKQdqPs",
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final words = TextFormField(
      controller: _wordsController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "OR_WORDLIST"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "NEW_PASSWORD"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    final passwordRepeat = TextFormField(
      controller: _passwordRepeatController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "NEW_PASSWORD_REPEAT"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    void onChange() {
      if (_usernameController.text.contains('@')) {
        _domainSuffix = '';
      } else {
        _domainSuffix = '@' + helper.getDomain(_serverUrlController.text);
      }

      setState(() {
        _domainSuffix = _domainSuffix;
      });
    }

    _usernameController.addListener(onChange);
    _serverUrlController.addListener(onChange);

    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    final requestPasswordResetButton = component.BtnPrimary(
      onPressed: () async {
        setState(() {
          _screen = 'loading';
        });
        try {
          await storage.write(
            key: 'serverUrl',
            value: _serverUrlController.text,
            iOptions: secureIOSOptions,
          );
          await storage.write(
            key: 'username',
            value: _usernameController.text + _domainSuffix,
            iOptions: secureIOSOptions,
          );

          reduxStore.dispatch(
            new InitiateLoginAction(
              _serverUrlController.text,
              _usernameController.text + _domainSuffix,
            ),
          );
          info = await apiClient.info();
        } on apiClient.ServiceUnavailableException {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        if (_usernameController.text == '') {
          setState(() {
            _screen = 'default';
          });
          return;
        }

        if (_wordsController.text == '' &&
            (_code1Controller.text == '' || _code2Controller.text == '')) {
          setState(() {
            _screen = 'default';
          });
          return;
        }

        String username = _usernameController.text + _domainSuffix;

        String testError = helper.isValidUsername(username);
        if (testError != null) {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        String recoveryCode;
        if (_wordsController.text != '') {
          recoveryCode = converter.hexToBase58(
              converter.wordsToHex(_wordsController.text.split(' ')));
        } else if (_code1Controller.text != '' && _code2Controller.text != '') {
          if (!await cryptoLibrary
                  .recoveryPasswordChunkPassChecksum(_code1Controller.text) ||
              !await cryptoLibrary
                  .recoveryPasswordChunkPassChecksum(_code2Controller.text)) {
            setState(() {
              _screen = 'default';
            });
            _showErrorDiaglog(
              FlutterI18n.translate(context, "ERROR"),
              FlutterI18n.translate(context, "AT_LEAST_ONE_CODE_INCORRECT"),
            );
            return;
          }
          recoveryCode = await cryptoLibrary.recoveryCodeStripChecksums(
              _code1Controller.text + _code2Controller.text);
        } else {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, "SOMETHING_STRANGE_HAPPENED"),
          );
          return;
        }

        RecoveryEnable data;
        try {
          data = await managerDatastoreUser.recoveryEnable(
              username, recoveryCode, _serverUrlController.text);
        } on apiClient.BadRequestException catch (e) {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog('ERROR', e.message);
          return;
        } catch (e) {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }
        setState(() {
          _recoveryCode = recoveryCode;
          _recoveryData = data;
          _screen = 'recovery_enabled';
        });
      },
      text: FlutterI18n.translate(context, "REQUEST_PASSWORD_RESET"),
    );

    final setNewPasswordButton = component.BtnPrimary(
      onPressed: () async {
        apiClient.Info info;
        try {
          info = await apiClient.info();
        } on apiClient.ServiceUnavailableException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        setState(() {
          _screen = 'loading';
        });

        String testError = helper.isValidPassword(
          _passwordController.text,
          _passwordRepeatController.text,
          info.complianceMinMasterPasswordLength,
          info.complianceMinMasterPasswordComplexity,
        );
        if (testError != null) {
          setState(() {
            _screen = 'recovery_enabled';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }
        String username = _usernameController.text + _domainSuffix;

        try {
          await managerDatastoreUser.setPassword(
            username,
            _recoveryCode,
            _passwordController.text,
            _recoveryData.userPrivateKey,
            _recoveryData.userSecretKey,
            _recoveryData.userSauce,
            _recoveryData.verifierPublicKey,
          );
        } on apiClient.ServiceUnavailableException {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          setState(() {
            _screen = 'default';
          });
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } on apiClient.BadRequestException catch (e) {
          _showErrorDiaglog('ERROR', e.message);
          setState(() {
            _screen = 'default';
          });
          return;
        }
        setState(() {
          _screen = 'success';
        });
      },
      text: FlutterI18n.translate(context, "SET_NEW_PASSWORD"),
    );

    final server = TextFormField(
      keyboardType: TextInputType.url,
      controller: _serverUrlController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "SERVER"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else if (_screen == 'success') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 24.0),
              new Icon(
                component.FontAwesome.thumbs_o_up,
                color: Color(0xFFFFFFFF),
                size: 48.0,
              ),
              SizedBox(height: 24.0),
              component.BtnPrimary(
                text: FlutterI18n.translate(context, "BACK_TO_HOME"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      );
    } else if (_screen == 'recovery_enabled') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 24.0),
              password,
              SizedBox(height: 8.0),
              passwordRepeat,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: setNewPasswordButton,
                  ),
                  Expanded(
                    child: FlatButton(
                      child: new Text(FlutterI18n.translate(context, "ABORT")),
                      textColor: Color(0xFFb1b6c1),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16.0),
              new Center(
                child: new RichText(
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                        style: new TextStyle(color: Color(0xFF666666)),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            final url = Uri.parse('https://www.psono.pw/privacy-policy.html');
                            if (await canLaunchUrl(url)) {
                              await launchUrl(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 24.0),
              username,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: code1,
                  ),
                  Expanded(
                    child: code2,
                  ),
                ],
              ),
              SizedBox(height: 8.0),
              words,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: requestPasswordResetButton,
                  ),
                  Expanded(
                    child: FlatButton(
                      child: new Text(FlutterI18n.translate(context, "ABORT")),
                      textColor: Color(0xFFb1b6c1),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 24.0),
              server,
              SizedBox(height: 16.0),
              new Center(
                child: new RichText(
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                        style: new TextStyle(color: Color(0xFF666666)),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            final url = Uri.parse('https://www.psono.pw/privacy-policy.html');
                            if (await canLaunchUrl(url)) {
                              await launchUrl(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
