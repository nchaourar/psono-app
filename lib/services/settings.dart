import 'package:psono/redux/store.dart';

String getSetting(String key) {
  if (key == null) {
    return null;
  }
  if (key == 'setting_password_length') {
    return reduxStore.state.passwordLength;
  }
  if (key == 'setting_password_letters_uppercase') {
    return reduxStore.state.lettersUppercase;
  }
  if (key == 'setting_password_letters_lowercase') {
    return reduxStore.state.lettersLowercase;
  }
  if (key == 'setting_password_numbers') {
    return reduxStore.state.numbers;
  }
  if (key == 'setting_password_special_chars') {
    return reduxStore.state.specialChars;
  }
  return null;
}
