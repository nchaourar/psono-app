// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oidc_initiate_login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OidcInitiateLogin _$OidcInitiateLoginFromJson(Map<String, dynamic> json) {
  return OidcInitiateLogin(
    oidcRedirectUrl: json['oidc_redirect_url'] as String,
  );
}

Map<String, dynamic> _$OidcInitiateLoginToJson(OidcInitiateLogin instance) =>
    <String, dynamic>{
      'oidc_redirect_url': instance.oidcRedirectUrl,
    };
