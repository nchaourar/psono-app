import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'oidc_login.g.dart';

@JsonSerializable()
class OidcLogin {
  OidcLogin({
    this.loginInfo,
    this.loginInfoNonce,
  });

  @JsonKey(name: 'login_info', fromJson: fromHex, toJson: toHex)
  final Uint8List loginInfo;
  @JsonKey(name: 'login_info_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List loginInfoNonce;

  factory OidcLogin.fromJson(Map<String, dynamic> json) =>
      _$OidcLoginFromJson(json);

  Map<String, dynamic> toJson() => _$OidcLoginToJson(this);
}
