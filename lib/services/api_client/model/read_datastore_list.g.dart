// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_datastore_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadDatastoreList _$ReadDatastoreListFromJson(Map<String, dynamic> json) {
  return ReadDatastoreList(
    datastores: (json['datastores'] as List)
        ?.map((e) => e == null
            ? null
            : ReadDatastoreListEntry.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ReadDatastoreListToJson(ReadDatastoreList instance) =>
    <String, dynamic>{
      'datastores': instance.datastores,
    };

ReadDatastoreListEntry _$ReadDatastoreListEntryFromJson(
    Map<String, dynamic> json) {
  return ReadDatastoreListEntry(
    id: json['id'] as String,
    type: json['type'] as String,
    description: json['description'] as String,
    isDefault: json['is_default'] as bool,
  );
}

Map<String, dynamic> _$ReadDatastoreListEntryToJson(
        ReadDatastoreListEntry instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'description': instance.description,
      'is_default': instance.isDefault,
    };
