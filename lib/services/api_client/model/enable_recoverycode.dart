import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'enable_recoverycode.g.dart';

@JsonSerializable()
class EnableRecoverycode {
  EnableRecoverycode({
    this.recoveryData,
    this.recoveryDataNonce,
    this.recoverySauce,
    this.userSauce,
    this.verifierPublicKey,
    this.verifierTimeValid,
  });

  @JsonKey(name: 'recovery_data', fromJson: fromHex, toJson: toHex)
  final Uint8List recoveryData;
  @JsonKey(name: 'recovery_data_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List recoveryDataNonce;
  @JsonKey(name: 'recovery_sauce')
  final String recoverySauce;
  @JsonKey(name: 'user_sauce')
  final String userSauce;
  @JsonKey(name: 'verifier_public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List verifierPublicKey;
  @JsonKey(name: 'verifier_time_valid')
  final int verifierTimeValid;

  factory EnableRecoverycode.fromJson(Map<String, dynamic> json) =>
      _$EnableRecoverycodeFromJson(json);

  Map<String, dynamic> toJson() => _$EnableRecoverycodeToJson(this);
}
