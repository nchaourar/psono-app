import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'read_share.g.dart';

@JsonSerializable()
class ReadShareRight {
  ReadShareRight({
    this.read,
    this.write,
    this.grant,
  });

  final bool read;
  final bool write;
  final bool grant;

  factory ReadShareRight.fromJson(Map<String, dynamic> json) =>
      _$ReadShareRightFromJson(json);

  Map<String, dynamic> toJson() => _$ReadShareRightToJson(this);
}

@JsonSerializable()
class ReadShare {
  ReadShare({
    this.id,
    this.data,
    this.dataNonce,
    this.userId,
    this.rights,
  });

  final String id;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List data;
  @JsonKey(name: 'data_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List dataNonce;
  final String userId;
  final ReadShareRight rights;

  factory ReadShare.fromJson(Map<String, dynamic> json) =>
      _$ReadShareFromJson(json);

  Map<String, dynamic> toJson() => _$ReadShareToJson(this);
}
