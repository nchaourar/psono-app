import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:psono/redux/store.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/device.dart' as device;
import 'model/_index.dart';
import 'exceptions.dart';

export 'model/_index.dart';
export 'exceptions.dart';

Future<http.Response> decryptData(
  sessionSecretKey,
  http.Response response,
) async {
  if (sessionSecretKey == null) {
    return response;
  }

  try {
    final Map bodyJson = jsonDecode(response.body);
    final String decryptedBody = await cryptoLibrary.decryptData(
      converter.fromHex(bodyJson['text']),
      converter.fromHex(bodyJson['nonce']),
      sessionSecretKey,
    );
    return http.Response(decryptedBody, response.statusCode);
  } catch (e) {
    return response;
  }
}

/// Small wrapper
Future<http.Response> call(String connectionType, String endpoint, Map data,
    Map<String, String> headers,
    [Uint8List sessionSecretKey]) async {
  final client = new http.Client();

  http.Request request = http.Request(
    connectionType,
    Uri.parse(
      reduxStore.state.serverUrl + endpoint,
    ),
  );

  if (data != null && sessionSecretKey != null && sessionSecretKey.length > 0) {
    final EncryptedData encryptedData = await cryptoLibrary.encryptData(
      jsonEncode(data),
      sessionSecretKey,
    );
    data = {
      'text': converter.toHex(encryptedData.text),
      'nonce': converter.toHex(encryptedData.nonce),
    };
  }
  if (data != null) {
    request.body = jsonEncode(data);
  }

  request.headers.addAll({
    'Content-Type': 'application/json',
  });

  if (sessionSecretKey != null &&
      sessionSecretKey.length > 0 &&
      headers != null &&
      headers.containsKey('Authorization')) {
    final String requestTime = helper.dateTimeToIso(new DateTime.now());
    final String requestDeviceFingerprint = await device.getDeviceFingerprint();
    final Map validator = {
      'request_time': requestTime,
      'request_device_fingerprint': requestDeviceFingerprint,
    };
    final EncryptedData encryptedValidator = await cryptoLibrary.encryptData(
      jsonEncode(validator),
      sessionSecretKey,
    );
    headers['Authorization-Validator'] = jsonEncode({
      'text': converter.toHex(encryptedValidator.text),
      'nonce': converter.toHex(encryptedValidator.nonce),
    });
  }

  if (headers != null) {
    request.headers.addAll(headers);
  }
  http.Response response = await http.Response.fromStream(
    await client.send(request),
  );

  return decryptData(
    sessionSecretKey,
    response,
  );
}

throwHttpException(int statusCode, message) {
  if (statusCode == 301 && message == '') {
    throw BadRequestException('ERROR_RECEIVED_301');
  }
  if (statusCode == 400) {
    throw BadRequestException(message);
  }
  if (statusCode == 401) {
    throw UnauthorizedException(message);
  }
  if (statusCode == 403) {
    throw ForbiddenException(message);
  }
  if (statusCode == 500) {
    throw InternalServerErrorException(message);
  }
  if (statusCode == 503) {
    throw ServiceUnavailableException(message);
  }

  throw UnknownException(message, statusCode);
}

void handleError(http.Response response) {
  String message = response.body;
  Map bodyJson;

  if (response.statusCode >= 500) {
    return throwHttpException(response.statusCode, response.body);
  }

  try {
    bodyJson = jsonDecode(response.body);
  } catch (e) {
    return throwHttpException(response.statusCode, response.body);
  }

  if (bodyJson.containsKey('non_field_errors')) {
    message = bodyJson['non_field_errors'][0];
  } else {
    return throwHttpException(response.statusCode, response.body);
  }

  return throwHttpException(response.statusCode, message);
}

/// Ajax GET request to get the server info
/// Returns a future with server's public information
Future<Info> info() async {
  final endpoint = '/info/';
  final connectionType = 'GET';
  final Map data = null;
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return Info.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return Info();
}

/// Ajax POST request to the backend with email and authkey for login
/// Returns a future with the login status
Future<Login> login(
  Uint8List loginInfo,
  Uint8List loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return Login.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return Login();
}

/// Ajax POST request to initiate the saml login
Future<SamlInitiateLogin> samlInitiateLogin(
  int samlProviderId,
  String returnToUrl,
) async {
  final connectionType = 'POST';
  final endpoint = '/saml/' + samlProviderId.toString() + '/initiate-login/';
  final Map data = {
    'return_to_url': returnToUrl,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return SamlInitiateLogin.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return SamlInitiateLogin();
}

/// Ajax POST request to initiate the saml login
Future<SamlLogin> samlLogin(
  Uint8List loginInfo,
  Uint8List loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  final connectionType = 'POST';
  final endpoint = '/saml/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return SamlLogin.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return SamlLogin();
}

/// Ajax POST request to initiate the OIDC login
Future<OidcInitiateLogin> oidcInitiateLogin(
  int oidcProviderId,
  String returnToUrl,
) async {
  final connectionType = 'POST';
  final endpoint = '/oidc/' + oidcProviderId.toString() + '/initiate-login/';
  final Map data = {
    'return_to_url': returnToUrl,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return OidcInitiateLogin.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return OidcInitiateLogin();
}

/// Ajax POST request to initiate the OIDC login
Future<OidcLogin> oidcLogin(
  Uint8List loginInfo,
  Uint8List loginInfoNonce,
  Uint8List publicKey,
  int sessionDuration,
) async {
  final connectionType = 'POST';
  final endpoint = '/oidc/login/';
  final Map data = {
    'login_info': converter.toHex(loginInfo),
    'login_info_nonce': converter.toHex(loginInfoNonce),
    'public_key': converter.toHex(publicKey),
    'session_duration': sessionDuration,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return OidcLogin.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return OidcLogin();
}

/// Ajax POST request to destroy the token and logout the user
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the login status
Future<Logout> logout(
  String token,
  Uint8List sessionSecretKey,
  String sessionId,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/logout/';
  final Map data = {
    'session_id': sessionId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return Logout.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return Logout();
}

/// AJAX POST request to the backend with the recovery_authkey to initiate the
/// reset of the password
Future<EnableRecoverycode> enableRecoverycode(
  String username,
  String recoveryAuthkey,
) async {
  final connectionType = 'POST';
  final endpoint = '/password/';
  final Map data = {
    'username': username,
    'recovery_authkey': recoveryAuthkey,
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return EnableRecoverycode.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return EnableRecoverycode();
}

/// AJAX POST request to the backend with the recovery_authkey to initiate the
/// reset of the password
Future<void> setPassword(
  String username,
  String recoveryAuthkey,
  Uint8List updateData,
  Uint8List updateDataNonce,
) async {
  final connectionType = 'PUT';
  final endpoint = '/password/';
  final Map data = {
    'username': username,
    'recovery_authkey': recoveryAuthkey,
    'update_data': converter.toHex(updateData),
    'update_data_nonce': converter.toHex(updateDataNonce),
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 200) {
    return;
  } else {
    handleError(response);
  }
  return;
}

/// Ajax POST request to destroy the token and logout the user
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the login status
Future<Logout> register(
  String email,
  String username,
  String authkey,
  Uint8List publicKey,
  Uint8List privateKey,
  Uint8List privateKeyNonce,
  Uint8List secretKey,
  Uint8List secretKeyNonce,
  String userSauce,
  String baseUrl,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/register/';
  final Map data = {
    'email': email,
    'username': username,
    'authkey': authkey,
    'public_key': converter.toHex(publicKey),
    'private_key': converter.toHex(privateKey),
    'private_key_nonce': converter.toHex(privateKeyNonce),
    'secret_key': converter.toHex(secretKey),
    'secret_key_nonce': converter.toHex(secretKeyNonce),
    'user_sauce': userSauce,
    'base_url': baseUrl
  };
  final Map<String, String> headers = null;

  final response = await call(connectionType, endpoint, data, headers);

  if (response.statusCode == 201) {
    return Logout.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return Logout();
}

/// Ajax POST request to activate the token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the rest of the login info
Future<ActivateToken> activateToken(
  String token,
  Uint8List verification,
  Uint8List verificationNonce,
  Uint8List sessionSecretKey,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/activate-token/';
  final Map data = {
    'verification': converter.toHex(verification),
    'verification_nonce': converter.toHex(verificationNonce),
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ActivateToken.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ActivateToken();
}

/// Ajax GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadDatastore> readDatastore(
  String token,
  Uint8List sessionSecretKey,
  String datastoreId,
) async {
  final connectionType = 'GET';
  final endpoint = '/datastore/' + datastoreId + '/';
  final Map data = null;
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ReadDatastore.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ReadDatastore();
}

/// Ajax GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadSecret> readSecret(
  String token,
  Uint8List sessionSecretKey,
  String secretId,
) async {
  final connectionType = 'GET';
  final endpoint = '/secret/' + secretId + '/';
  final Map data = null;
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ReadSecret.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ReadSecret();
}

/// Ajax PUT request to create a secret with the token as authentication together with the encrypted data and nonce
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the secret id
Future<CreateSecret> createSecret(
  String token,
  Uint8List sessionSecretKey,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
  String linkId,
  String parentDatastoreId,
  String parentShareId,
  String callbackUrl,
  String callbackUser,
  String callbackPass,
) async {
  final connectionType = 'PUT';
  final endpoint = '/secret/';
  final Map data = {
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'link_id': linkId,
    'callback_url': callbackUrl,
    'callback_user': callbackUser,
    'callback_pass': callbackPass,
  };

  if (parentDatastoreId != null) {
    data['parent_datastore_id'] = parentDatastoreId;
  }

  if (parentShareId != null) {
    data['parent_share_id'] = parentShareId;
  }

  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 201) {
    return CreateSecret.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return CreateSecret();
}

/// Ajax PUT request with the token as authentication and the new secret content
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the secret id
Future<WriteSecret> writeSecret(
  String token,
  Uint8List sessionSecretKey,
  String secretId,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
  String callbackUrl,
  String callbackUser,
  String callbackPass,
) async {
  final connectionType = 'POST';
  final endpoint = '/secret/';
  final Map data = {
    'secret_id': secretId,
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'callback_url': callbackUrl,
    'callback_user': callbackUser,
    'callback_pass': callbackPass,
  };

  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return WriteSecret.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return WriteSecret();
}

/// Ajax GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
Future<ReadDatastoreList> readDatastoreList(
  String token,
  Uint8List sessionSecretKey,
) async {
  final connectionType = 'GET';
  final endpoint = '/datastore/';
  final Map data = null;
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ReadDatastoreList.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ReadDatastoreList();
}

/// Ajax GET request with the [token] as authentication to get all the users share rights.
/// The request is encrypted with the [sessionSecretKey]
Future<ReadShareRightsList> readShareRightsOverview(
  String token,
  Uint8List sessionSecretKey,
) async {
  final connectionType = 'GET';
  final endpoint = '/share/right/';
  final Map data = null;
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ReadShareRightsList.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ReadShareRightsList();
}

/// Ajax DELETE request with the token as authentication to delete a user account
Future<DeleteAccount> deleteAccount(
  String token,
  Uint8List sessionSecretKey,
  String authkey,
) async {
  final connectionType = 'DELETE';
  final endpoint = '/user/delete/';
  final Map data = {
    'authkey': authkey,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return DeleteAccount();
  } else {
    handleError(response);
  }
  return DeleteAccount();
}

/// Ajax GET request with the token as authentication to get the current user's datastore
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the encrypted datastore
Future<CreateDatastore> createDatastore(
  String token,
  Uint8List sessionSecretKey,
  String type,
  String description,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
  bool isDefault,
  Uint8List secretKey,
  Uint8List secretKeyNonce,
) async {
  final connectionType = 'PUT';
  final endpoint = '/datastore/';
  final Map data = {
    'type': type,
    'description': description,
    'data': converter.toHex(encryptedData),
    'data_nonce': converter.toHex(encryptedDataNonce),
    'is_default': isDefault,
    'secret_key': converter.toHex(secretKey),
    'secret_key_nonce': converter.toHex(secretKeyNonce),
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 201) {
    return CreateDatastore.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return CreateDatastore();
}

/// Ajax POST request with the token as authentication and the datastore's new content
Future<void> writeDatastore(
  String token,
  Uint8List sessionSecretKey,
  String datastoreId,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
  Uint8List encryptedDataSecretKey,
  Uint8List encryptedDataSecretKeyNonce,
  String description,
  bool isDefault,
) async {
  final connectionType = 'POST';
  final endpoint = '/datastore/';
  final Map data = {
    'datastore_id': datastoreId,
  };

  if (encryptedData != null) {
    data['data'] = converter.toHex(encryptedData);
  }

  if (encryptedDataNonce != null) {
    data['data_nonce'] = converter.toHex(encryptedDataNonce);
  }

  if (encryptedDataSecretKey != null) {
    data['secret_key'] = converter.toHex(encryptedDataSecretKey);
  }

  if (encryptedDataSecretKeyNonce != null) {
    data['secret_key_nonce'] = converter.toHex(encryptedDataSecretKeyNonce);
  }

  if (isDefault != null) {
    data['is_default'] = isDefault;
  }

  if (description != null) {
    data['description'] = description;
  }

  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return;
  } else {
    handleError(response);
  }
  return;
}

/// Ajax POST request to the backend with the YubiKey OTP Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future the verification status
Future<YubikeyOtpVerify> yubikeyOtpVerify(
  String token,
  Uint8List sessionSecretKey,
  String yubikeyOtp,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/yubikey-otp-verify/';
  final Map data = {
    'yubikey_otp': yubikeyOtp,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return YubikeyOtpVerify();
  } else {
    handleError(response);
  }
  return YubikeyOtpVerify();
}

/// Ajax POST request to the backend with the OATH-TOTP Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future the verification status
Future<GaVerify> gaVerify(
  String token,
  Uint8List sessionSecretKey,
  String gaToken,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/ga-verify/';
  final Map data = {
    'ga_token': gaToken,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return GaVerify();
  } else {
    handleError(response);
  }
  return GaVerify();
}

/// Ajax POST request to the backend with the Duo Token
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the verification status
Future<DuoVerify> duoVerify(
  String token,
  Uint8List sessionSecretKey,
  String duoToken,
) async {
  final connectionType = 'POST';
  final endpoint = '/authentication/duo-verify/';
  Map data;
  if (duoToken != '') {
    data = {
      'duo_token': duoToken,
    };
  }
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return DuoVerify();
  } else {
    handleError(response);
  }
  return DuoVerify();
}

/// Ajax GET request with the token as authentication to get the content for a single share
/// The request is encrypted with the [sessionSecretKey]
/// Returns a future with the encrypted share content
Future<ReadShare> readShare(
  String token,
  Uint8List sessionSecretKey,
  String shareId,
) async {
  final connectionType = 'GET';
  final endpoint = '/share/' + shareId + '/';
  final Map data = null;
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return ReadShare.fromJson(
      jsonDecode(response.body),
    );
  } else {
    handleError(response);
  }
  return ReadShare();
}

/// Ajax PUT request with the token as authentication and the share's new content
Future<void> writeShare(
  String token,
  Uint8List sessionSecretKey,
  String shareId,
  Uint8List encryptedData,
  Uint8List encryptedDataNonce,
) async {
  final connectionType = 'PUT';
  final endpoint = '/share/';
  final Map data = {
    'share_id': shareId,
  };

  if (encryptedData != null) {
    data['data'] = converter.toHex(encryptedData);
  }

  if (encryptedDataNonce != null) {
    data['data_nonce'] = converter.toHex(encryptedDataNonce);
  }

  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return;
  } else {
    handleError(response);
  }
  return;
}

/// DELETE request with the token as authentication to delete a share link
Future<DeleteShareLink> deleteShareLink(
  String token,
  Uint8List sessionSecretKey,
  String linkId,
) async {
  final connectionType = 'DELETE';
  final endpoint = '/share/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return DeleteShareLink();
  } else {
    handleError(response);
  }
  return DeleteShareLink();
}

/// DELETE request with the token as authentication to delete a secret link
Future<DeleteSecretLink> deleteSecretLink(
  String token,
  Uint8List sessionSecretKey,
  String linkId,
) async {
  final connectionType = 'DELETE';
  final endpoint = '/secret/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return DeleteSecretLink();
  } else {
    handleError(response);
  }
  return DeleteSecretLink();
}

/// DELETE request with the token as authentication to delete a secret link
Future<DeleteFileLink> deleteFileLink(
  String token,
  Uint8List sessionSecretKey,
  String linkId,
) async {
  final connectionType = 'DELETE';
  final endpoint = '/file/link/';
  final Map data = {
    'link_id': linkId,
  };
  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return DeleteFileLink();
  } else {
    handleError(response);
  }
  return DeleteFileLink();
}

/// AJAX PUT request to the backend with new user informations like for example a new password (means new
/// authkey) or new public key
Future<void> updateUser(
  String token,
  Uint8List sessionSecretKey,
  String email,
  String authkey,
  String authkeyOld,
  Uint8List privateKey,
  Uint8List privateKeyNonce,
  Uint8List secretKey,
  Uint8List secretKeyNonce,
) async {
  final connectionType = 'PUT';
  final endpoint = '/user/update/';
  final Map data = {};

  if (email != null) {
    data['email'] = email;
  }

  if (authkey != null) {
    data['authkey'] = authkey;
  }

  if (authkeyOld != null) {
    data['authkey_old'] = authkeyOld;
  }

  if (privateKey != null) {
    data['private_key'] = converter.toHex(privateKey);
  }

  if (privateKeyNonce != null) {
    data['private_key_nonce'] = converter.toHex(privateKeyNonce);
  }

  if (secretKey != null) {
    data['secret_key'] = converter.toHex(secretKey);
  }

  if (secretKeyNonce != null) {
    data['secret_key_nonce'] = converter.toHex(secretKeyNonce);
  }

  final Map<String, String> headers = {
    "Authorization": "Token " + token,
  };

  final response = await call(
    connectionType,
    endpoint,
    data,
    headers,
    sessionSecretKey,
  );

  if (response.statusCode == 200) {
    return;
  } else {
    handleError(response);
  }
  return;
}
