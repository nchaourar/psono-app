import 'dart:typed_data';
import 'package:redux/redux.dart';

import 'package:psono/model/app_state.dart';
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';

// serverUrlReducer
String setServerUrlReducer(String state, action) {
  return action.serverUrl;
}

final Reducer<String> serverUrlReducer = combineReducers<String>([
  new TypedReducer<String, InitiateLoginAction>(setServerUrlReducer),
  new TypedReducer<String, InitiateStateAction>(setServerUrlReducer),
]);

// verifyKeyOldReducer
Uint8List setVerifyKeyOldReducer(Uint8List state, action) {
  return action.verifyKeyOld;
}

final Reducer<Uint8List> verifyKeyOldReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetVerifyKeyAction>(setVerifyKeyOldReducer),
]);

// verifyKeyReducer
Uint8List setVerifyKeyReducer(Uint8List state, action) {
  return action.verifyKey;
}

final Reducer<Uint8List> verifyKeyReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetVerifyKeyAction>(setVerifyKeyReducer),
]);

// usernameReducer
String setUsernameReducer(String state, action) {
  return action.username;
}

final Reducer<String> usernameReducer = combineReducers<String>([
  new TypedReducer<String, InitiateLoginAction>(setUsernameReducer),
  new TypedReducer<String, InitiateStateAction>(setUsernameReducer),
  new TypedReducer<String, SetUserUsernameAction>(setUsernameReducer),
]);

//// passwordReducer
//String setPasswordReducer(String state, action) {
//  return action.password;
//}
//
//final Reducer<String> passwordReducer = combineReducers<String>([
//  new TypedReducer<String, InitiateLoginAction>(setPasswordReducer),
//]);

// tokenReducer
String setTokenReducer(String state, action) {
  return action.token;
}

final Reducer<String> tokenReducer = combineReducers<String>([
  new TypedReducer<String, SetSessionInfoAction>(setTokenReducer),
  new TypedReducer<String, InitiateStateAction>(setTokenReducer),
]);

// sessionSecretKeyReducer
Uint8List setSessionSecretKeyReducer(Uint8List state, action) {
  return action.sessionSecretKey;
}

final Reducer<Uint8List> sessionSecretKeyReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetSessionInfoAction>(setSessionSecretKeyReducer),
  new TypedReducer<Uint8List, InitiateStateAction>(setSessionSecretKeyReducer),
]);

// secretKeyReducer
Uint8List setSecretKeyReducer(Uint8List state, action) {
  return action.secretKey;
}

final Reducer<Uint8List> secretKeyReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetSessionInfoAction>(setSecretKeyReducer),
  new TypedReducer<Uint8List, InitiateStateAction>(setSecretKeyReducer),
]);

// publicKeyReducer
Uint8List setPublicKeyReducer(Uint8List state, action) {
  return action.publicKey;
}

final Reducer<Uint8List> publicKeyReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetSessionInfoAction>(setPublicKeyReducer),
  new TypedReducer<Uint8List, InitiateStateAction>(setPublicKeyReducer),
]);

// privateKeyReducer
Uint8List setPrivateKeyReducer(Uint8List state, action) {
  return action.privateKey;
}

final Reducer<Uint8List> privateKeyReducer = combineReducers<Uint8List>([
  new TypedReducer<Uint8List, SetSessionInfoAction>(setPrivateKeyReducer),
  new TypedReducer<Uint8List, InitiateStateAction>(setPrivateKeyReducer),
]);

// lockscreenPassphraseReducer
String setLockscreenPassphraseReducer(String state, action) {
  return action.lockscreenPassphrase;
}

final Reducer<String> lockscreenPassphraseReducer = combineReducers<String>([
  new TypedReducer<String, UpdateLockscreenSettingAction>(
      setLockscreenPassphraseReducer),
  new TypedReducer<String, InitiateStateAction>(setLockscreenPassphraseReducer),
]);

// lockscreenEnabledReducer
bool setLockscreenEnabledReducer(bool state, action) {
  return action.lockscreenEnabled;
}

final Reducer<bool> lockscreenEnabledReducer = combineReducers<bool>([
  new TypedReducer<bool, UpdateLockscreenSettingAction>(
      setLockscreenEnabledReducer),
  new TypedReducer<bool, InitiateStateAction>(setLockscreenEnabledReducer),
]);

// complianceDisableDeleteAccountReducer
bool setComplianceDisableDeleteAccountReducer(bool state, action) {
  return action.complianceDisableDeleteAccount;
}

final Reducer<bool> complianceDisableDeleteAccountReducer =
    combineReducers<bool>([
  new TypedReducer<bool, InitiateStateAction>(
      setComplianceDisableDeleteAccountReducer),
  new TypedReducer<bool, SetVerifyKeyAction>(
      setComplianceDisableDeleteAccountReducer),
]);

// userIdReducer
String setUserIdReducer(String state, action) {
  return action.userId;
}

final Reducer<String> userIdReducer = combineReducers<String>([
  new TypedReducer<String, SetSessionInfoAction>(setUserIdReducer),
  new TypedReducer<String, InitiateStateAction>(setUserIdReducer),
]);

// userEmailReducer
String setUserEmailReducer(String state, action) {
  return action.userEmail;
}

final Reducer<String> userEmailReducer = combineReducers<String>([
  new TypedReducer<String, SetSessionInfoAction>(setUserEmailReducer),
  new TypedReducer<String, InitiateStateAction>(setUserEmailReducer),
  new TypedReducer<String, SetUserEmailAction>(setUserEmailReducer),
]);

// userSauceReducer
String setUserSauceReducer(String state, action) {
  return action.userSauce;
}

final Reducer<String> userSauceReducer = combineReducers<String>([
  new TypedReducer<String, SetSessionInfoAction>(setUserSauceReducer),
  new TypedReducer<String, InitiateStateAction>(setUserSauceReducer),
]);

// configReducer
Config setConfigJsonReducer(Config state, action) {
  return action.config;
}

final Reducer<Config> configReducer = combineReducers<Config>([
  new TypedReducer<Config, InitiateStateAction>(setConfigJsonReducer),
  new TypedReducer<Config, ConfigUpdatedAction>(setConfigJsonReducer),
]);

// passwordLengthReducer
String setPasswordLengthReducer(String state, action) {
  return action.passwordLength;
}

final Reducer<String> passwordLengthReducer = combineReducers<String>([
  new TypedReducer<String, PasswordGeneratorSettingAction>(
      setPasswordLengthReducer),
]);

// lettersUppercaseReducer
String setLettersUppercaseReducer(String state, action) {
  return action.lettersUppercase;
}

final Reducer<String> lettersUppercaseReducer = combineReducers<String>([
  new TypedReducer<String, PasswordGeneratorSettingAction>(
      setLettersUppercaseReducer),
]);

// lettersLowercaseReducer
String setLettersLowercaseReducer(String state, action) {
  return action.lettersLowercase;
}

final Reducer<String> lettersLowercaseReducer = combineReducers<String>([
  new TypedReducer<String, PasswordGeneratorSettingAction>(
      setLettersLowercaseReducer),
]);

// numbersReducer
String setNumbersReducer(String state, action) {
  return action.numbers;
}

final Reducer<String> numbersReducer = combineReducers<String>([
  new TypedReducer<String, PasswordGeneratorSettingAction>(setNumbersReducer),
]);

// specialCharsReducer
String setSpecialCharsReducer(String state, action) {
  return action.specialChars;
}

final Reducer<String> specialCharsReducer = combineReducers<String>([
  new TypedReducer<String, PasswordGeneratorSettingAction>(
      setSpecialCharsReducer),
]);

AppState appReducer(AppState state, action) {
  return AppState(
    serverUrl: serverUrlReducer(state.serverUrl, action),
    verifyKeyOld: verifyKeyOldReducer(state.verifyKeyOld, action),
    verifyKey: verifyKeyReducer(state.verifyKey, action),
    username: usernameReducer(state.username, action),
    //password: passwordReducer(state.password, action),
    token: tokenReducer(state.token, action),
    sessionSecretKey: sessionSecretKeyReducer(state.sessionSecretKey, action),
    secretKey: secretKeyReducer(state.secretKey, action),
    publicKey: publicKeyReducer(state.publicKey, action),
    privateKey: privateKeyReducer(state.privateKey, action),
    lockscreenPassphrase:
        lockscreenPassphraseReducer(state.lockscreenPassphrase, action),
    lockscreenEnabled:
        lockscreenEnabledReducer(state.lockscreenEnabled, action),
    complianceDisableDeleteAccount: complianceDisableDeleteAccountReducer(
        state.complianceDisableDeleteAccount, action),
    userId: userIdReducer(state.userId, action),
    userEmail: userEmailReducer(state.userEmail, action),
    userSauce: userSauceReducer(state.userSauce, action),
    config: configReducer(state.config, action),

    passwordLength: passwordLengthReducer(state.passwordLength, action),
    lettersUppercase: lettersUppercaseReducer(state.lettersUppercase, action),
    lettersLowercase: lettersLowercaseReducer(state.lettersLowercase, action),
    numbers: numbersReducer(state.numbers, action),
    specialChars: specialCharsReducer(state.specialChars, action),
  );
}
