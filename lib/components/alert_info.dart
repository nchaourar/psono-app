import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AlertInfo extends StatelessWidget {
  final String text;

  AlertInfo({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
      decoration: new BoxDecoration(
        color: Color(0xFFd9edf7),
        border: Border.all(
          width: 1.0,
          color: Color(0xFFbce8f1),
        ),
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
        ),
      ),
      child: RichText(
        text: TextSpan(
          text: FlutterI18n.translate(context, "INFO") + '! ',
          style: TextStyle(
            color: Color(0xFF31708f),
            fontWeight: FontWeight.bold,
          ),
          children: <TextSpan>[
            TextSpan(
              text: text,
              style: TextStyle(
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
