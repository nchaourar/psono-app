import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:psono/page_route_no_transition.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/passphrase/index.dart';
import 'package:psono/services/storage.dart';

class ProtectedScreen extends StatefulWidget {
  final Widget child;
  ProtectedScreen({Key key, this.child}) : super(key: key);
  _ProtectedScreenState createState() => _ProtectedScreenState();
}

class _ProtectedScreenState extends State<ProtectedScreen>
    with WidgetsBindingObserver {
  int pauseEnabledTime;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (reduxStore.state.serverUrl == null ||
          reduxStore.state.serverUrl == '' ||
          reduxStore.state.token == null ||
          reduxStore.state.token == '' ||
          reduxStore.state.sessionSecretKey == null ||
          reduxStore.state.sessionSecretKey == '') {
        Navigator.pushReplacementNamed(context, '/');
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    int lockscreenThreshhold =
        3 * 60 * 1000; // will lock the screen after 3 minutes

    String lastUnlockTimeString = await storage.read(key: 'lastUnlockTime');
    int lastUnlockTimeInt =
        lastUnlockTimeString != null ? int.parse(lastUnlockTimeString) : 0;

    if (reduxStore.state.lockscreenEnabled &&
        state == AppLifecycleState.resumed &&
        pauseEnabledTime != null &&
        (DateTime.now().millisecondsSinceEpoch - lastUnlockTimeInt) > 3000 &&
        (DateTime.now().millisecondsSinceEpoch - pauseEnabledTime) >
            lockscreenThreshhold) {
      Navigator.of(context).push(
        NoTransitionPageRoute(builder: (context) => PassphraseScreen()),
      );
    }
    if (state == AppLifecycleState.paused) {
      pauseEnabledTime = DateTime.now().millisecondsSinceEpoch;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }
}
