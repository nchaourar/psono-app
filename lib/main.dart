import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:sentry/sentry.dart';
import 'package:flutter/material.dart';
import 'dart:async';

Future<void> main() async {
  await Sentry.init(
    (options) {
      options.dsn =
          'https://7b17edbedc9244da8c16f3c30db4b0fe@sentry.io/1527081';
    },
    appRunner: initApp, // Init your App.
  );
}

void initApp() {
  var configuredApp = new AppConfig(
    appName: 'Psono',
    flavorName: 'production',
    child: new MyApp(),
  );
  runApp(configuredApp);
}
