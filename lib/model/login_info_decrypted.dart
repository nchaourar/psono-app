import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';

part 'login_info_decrypted.g.dart';

@JsonSerializable()
class LoginInfoDecrypted {
  LoginInfoDecrypted({
    this.data,
    this.dataNonce,
    this.serverSessionPublicKey,
  });

  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List data;
  @JsonKey(name: 'data_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List dataNonce;
  @JsonKey(name: 'server_session_public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List serverSessionPublicKey;

  factory LoginInfoDecrypted.fromJson(Map<String, dynamic> json) =>
      _$LoginInfoDecryptedFromJson(json);

  Map<String, dynamic> toJson() => _$LoginInfoDecryptedToJson(this);
}
